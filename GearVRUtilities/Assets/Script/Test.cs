﻿using UnityEngine;
using System.Collections;
using Tamago.Unity.Utilities;

public class Test : MonoBehaviour {

	GearVRInputManager gearVRInputManager;

	// Use this for initialization
	void Start () {
		gearVRInputManager = GetComponent<GearVRInputManager>();
		gearVRInputManager.BackEvent += () => {
			Debug.Log ("back");
		};
		gearVRInputManager.BackHomeEvent += () => {
			Debug.Log("back home");
		};
		gearVRInputManager.TouchMoveEvent += (pivotDistance) => {
			Debug.Log (pivotDistance);
		};
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
