﻿using UnityEngine;
using System.Collections;

namespace Tamago.Unity.Utilities{
	[RequireComponent(typeof (PointerManager))]
	public class GearVRInputManager : MonoBehaviour {

		public float BackHomeTime = 4f;
		float BackStartTime;
		Vector2 startPosition;
		PointerManager pointerManager;

		//event
		public delegate void BackEventHandler();
		public event BackEventHandler BackEvent;
		public delegate void BackHomeEventHandler();
		public event BackHomeEventHandler BackHomeEvent;
		public delegate void MoveEventHandler(Vector2 pivotDistance);
		public event MoveEventHandler TouchMoveEvent;

		// Use this for initialization
		void Awake () {
			pointerManager = GetComponent<PointerManager>();
			InitTouchMoveEvent();
		}

		public void ResetTouchMoveEvent(){
			InitTouchMoveEvent();
		}
		
		// Update is called once per frame
		void Update () {
			if(Input.GetKeyDown(KeyCode.Escape)){
				BackStartTime = Time.time;
			}
			if(Input.GetKey(KeyCode.Escape)){
				if(Time.time - BackStartTime > BackHomeTime){
					if(BackHomeEvent != null){
						BackHomeEvent();
					}
				}
			}
			if(Input.GetKeyUp (KeyCode.Escape)){
				if(BackEvent != null){
					BackEvent();
				}
			}
			if(Input.GetMouseButtonDown(0)){
				pointerManager.PointerDown(0, Input.mousePosition);
			}
			if(Input.GetMouseButtonUp(0)){
				pointerManager.PointerUp(0, Input.mousePosition);
			}
			if(Input.GetMouseButton(0)){
				pointerManager.PointerDown(0, Input.mousePosition);
			}
		}

		void InitTouchMoveEvent(){
			pointerManager.DownEvent += (position) => {
				startPosition = position;
			};
			pointerManager.DownMoveEvent += (position, deltaPosition) => {
				var distance = position - startPosition;
				if(TouchMoveEvent != null){
					TouchMoveEvent(distance);
				}
			};
		}

		public PointerManager PointerManager{
			get{return pointerManager;}
		}

	}
}

